package inheritence;

public class Batch extends CSDStudent {

	private int batch;

	Batch(String name, int age, String NIC, String manager, String CName, String department, int batch) {
		super(name, age, NIC, manager, CName, department);
		this.batch = batch;
	}

	void bDisplay() {
		System.out.println("Batch:"+batch);
	}

	public static void main(String[] arg) {
		Batch student = new Batch("vino", 23, "967224391v", "praveen", "Jaffna Campus", "CSD", 13);
		student.getStudentDetails();
		student.BcasStudent();
		student.depDisplay();
		student.bDisplay();
	}

}
