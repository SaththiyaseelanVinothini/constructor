package inheritence;

public class Student {
	private String name;
	private int age;
	private String NIC;

	Student(String name, int age, String NIC) {
		this.name = name;
		this.age = age;
		this.NIC = NIC;
	}

	 void getStudentDetails() {
		System.out.println("Name: " + name + " " + "Age: " + this.age + " " + "NICNo:" + " " + this.NIC);
	}

}
